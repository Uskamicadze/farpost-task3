﻿select id, name from (select id, name,
	now() - date_created as "was_created", 
	now() - date_owner as "WTF?", 
	now() - date_updated as "was_updated", 
	now() - owner_visit_date as "was_visited",
	now() - last_change as "was_changed"
	 from companies 
	order by id) as n
where "was_updated" > interval '1 year' and "was_visited" > interval '1 year' and "was_changed" > interval '1 year'
union
select id, name, admin_note, "last_ch", "last_dif" from (select c.id, c.name, c.admin_note, now() - max(c_ch.created) as "last_ch", now() - max(c_d.created) as "last_dif", c.last_change from companies c
inner join content_changes  c_ch on c_ch.content_id = c.id
inner join content_diffs c_d on c_d.content_id = c_ch.id
group by c.id) as n
where "last_ch" > interval '1 year' or "last_dif" > interval '1 year'